package com.junojuno.vote.etc;

import java.util.HashMap;
import java.util.Map;

public class Parser {
	public static Map<String, String> requestBodyStringToMap(String requestBody) {
		HashMap<String, String> map = new HashMap<String, String>();
		
		String[] reqBodies = requestBody.split("&");
		
		for (int i = 0 ; i < reqBodies.length ; i++) {
			String[] keyVal = reqBodies[i].split("=");
			map.put(keyVal[0], keyVal[1]);
		}
		
		return map;
	}
}
