package com.junojuno.vote.etc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.junojuno.vote.controller.HashController;
import com.junojuno.vote.service.VoterServiceImpl;

public class FingerMatcher
{

//
//   public static void main(String[] args)
//   {
//      
//      System.out.println(rad2deg(5.90));
//      System.out.println(deg2rad(338.045));
//      Minutia[] minutiae;
//      minutiae= new Minutia[100];
//      minutiae[0] = new Minutia(1,2,3,4.2);
//      
//      List<Minutia> mm =new ArrayList<Minutia>();
//      mm.add(new Minutia(1,2,3,4));
//   
//      
//      
//       double[] locationPara=locationChangeparam(73,135,253,89,45,111,222,62);
//       double[] xaxs= locationChange(3,4,locationPara);
//       
//   }

	private static final Logger logger = LoggerFactory.getLogger(FingerMatcher.class);

	   
	   
	   public static Nearest2feature[] nearest2featureExtract(Minutia[] minutiae)
	   {
	      double mini_distance1=300;
	      double mini_distance2=300;
	      int mini_x1=0;
	      int mini_x2=0;
	      int mini_y2=0;
	      int mini_y1=0;
	      double temp;
	      int index1;
	      int index2;
	      Nearest2feature[] result;
	      result = new Nearest2feature[minutiae.length];
	      
	      for(int i=0; i<minutiae.length-1; i++)
	      {
	         mini_distance1=300;
	         mini_distance2=300;
	         mini_x1=0;
	         mini_x2=0;
	         mini_y2=0;
	         mini_y1=0;
	         index1=0;
	         index2=0;
	         for(int j=i+1; j<minutiae.length;j++)
	         {
	            temp=distancebetween(minutiae[i].x,minutiae[i].y,minutiae[j].x,minutiae[j].y);
	            if(temp<=mini_distance1)
	            {
	               mini_distance2=mini_distance1;
	               mini_x2=mini_x1;
	               mini_y2=mini_y1;
	               index2=index1;;
	               
	               mini_distance1=temp;
	               mini_x1=minutiae[j].x;
	               mini_y1=minutiae[j].y;
	               index1=j;
	               
	            }
	            else if ((temp>mini_distance1)&&(temp>mini_distance2))
	            {
	               mini_distance2=temp;
	               index2=j;
	            }
	            else
	            {
	               
	            }
	         }
	         
	         
	         
	         result[i]=new Nearest2feature(minutiae[i].x,minutiae[i].y,mini_x1,mini_y1, mini_x2, mini_y2,mini_distance1,mini_distance1,minutiae[i].angle,minutiae[index1].angle,minutiae[index2].angle,minutiae[i].MinutiaType,minutiae[index1].MinutiaType,minutiae[index2].MinutiaType );
	      }
	      return result;
	   }
	   
	   public static double localscore(Nearest2feature origin,Nearest2feature template)
	   {
	      double [] result= new double[7];
	      
	      result[0]=Math.abs(origin.distance_1-template.distance_1);
	      result[1]=Math.abs(origin.distance_2-template.distance_2);
	      result[2]=Math.abs(origin.angle_me_1-template.angle_me_1);
	      result[3]=Math.abs(origin.angle_me_2-template.angle_me_2);
	      if(origin.type==template.type)
	      {   
	         result[4]=1;
	      }
	      else
	      {
	         result[4]=0;
	      }
	      
	      if(origin.type1==template.type1)
	      {   
	         result[5]=1;
	      }
	      else
	      {
	         result[5]=0;
	      }
	      
	      if(origin.type2==template.type2)
	      {   
	         result[6]=1;
	      }
	      else
	      {
	         result[6]=0;
	      }
	      
	      return -result[0]-result[1]-(57.2958*0.3*result[2])-(57.2958*0.3*result[3])+result[4]*3+result[5]*3+result[6]*3;
	   }
	   
	   public static double calMatchScore(Minutia ori1,Minutia ori2,Minutia temp1,Minutia temp2)
	   {
	      double ori_dist;
	      double temp_dist;
	      double ori_angle;
	      double temp_angle;
	      int typeMat1;
	      int typeMat2;
	      
	      ori_dist=Math.sqrt(Math.abs((ori1.x-ori2.x)*(ori1.x-ori2.x)+(ori1.y-ori2.y)*(ori1.y-ori2.y)));
	      temp_dist=Math.sqrt(Math.abs((temp1.x-temp2.x)*(temp1.x-temp2.x)+(temp1.y-temp2.y)*(temp1.y-temp2.y)));
	      ori_angle=Math.abs(ori1.angle-ori2.angle);
	      temp_angle=Math.abs(temp1.angle-temp2.angle);
	      if(ori1.MinutiaType==temp1.MinutiaType)
	      {
	         typeMat1=1;
	      }
	      else
	      {
	         typeMat1=0;
	      }
	      if(ori2.MinutiaType==temp2.MinutiaType)
	      {
	         typeMat2=1;
	      }
	      else
	      {
	         typeMat2=0;
	      }
	      //System.out.println("@@@@ : "+Math.abs(ori_dist-temp_dist*-1+-1*0.3*57.2958*(ori_angle-temp_angle)+typeMat1*3+typeMat2*3));
	      return Math.abs(ori_dist-temp_dist*-1+-1*0.3*57.2958*(ori_angle-temp_angle)+typeMat1*3+typeMat2*3);
	   }

			
	   public static int FgFeatureCompare(FgFeature a, FgFeature b)
	   {
	      if((Math.abs(a.distance-b.distance)<8)&&(Math.abs(a.angle-b.angle)*57.2958<30))
	      {
	         return 1;
	      }
	      else
	      {
	         return 0;
	      }
	   }
	      public static int minutiaeMatching(Minutia[] origin, Minutia[] template,double Ms)
	      {
	         
	         double[] abcd={0,0,0,0};
	         double[] temp_changed_origin={0,0};
	         Minutia[] changed_origin = new Minutia[origin.length];
	         
	         double maxScore=0;
	         double scoreTemp=0;
	         for(int i = 0; i < origin.length; i++) {
	            changed_origin[i] = new Minutia(origin[i].x, origin[i].y,origin[i].MinutiaType,origin[i].angle);
	         }
	         
	         int sum=0;
	         double howmany=0.4;
	         
	         
	         for (int ori_i=0; ori_i<origin.length;ori_i++)
	         {
	            for (int ori_j=0; ori_j<origin.length;ori_j++)
	            {
	               if (ori_j!=ori_i)
	               {
	                  for(int temp_i=0; temp_i<template.length;temp_i++)
	                  {
	                     for(int temp_j=0; temp_j<template.length;temp_j++)
	                     {
	                        if(temp_i!=temp_j)
	                        {
	                           scoreTemp=calMatchScore(origin[ori_i],origin[ori_j],template[temp_i],template[temp_j]);
	                           if(scoreTemp>maxScore)
	                           {
	                              maxScore=scoreTemp;
	                              //System.out.print("score : ");
	                              //System.out.println(maxScore);
	                              
	                              
	                           }
	                           
	                           if(scoreTemp>400)
	                           {
	                              FgFeature[] FgFeatures1 = new FgFeature[origin.length];
	                              
	                              for(int i = 0; i < origin.length; i++) {
	                                 FgFeatures1[i] = new FgFeature(distancebetween(origin[ori_i].x,origin[ori_i].x,origin[i].x,origin[i].y),Math.abs(origin[ori_i].angle-origin[ori_i].angle));
	                                }
	                              FgFeature[] FgFeatures2 = new FgFeature[template.length];
	                              for(int i = 0; i < template.length; i++) {
	                                 FgFeatures2[i] = new FgFeature(distancebetween(template[temp_i].x,template[temp_i].x,template[i].x,template[i].y),Math.abs(template[temp_i].angle-template[temp_i].angle));
	                                }
	                              for(int i= 0; i<FgFeatures1.length;i++)
	                              {
	                                 int summation=0;
	                                 for(int j=0; j<FgFeatures2.length;j++)
	                                 {
	                                    if(FgFeatureCompare(FgFeatures1[i], FgFeatures2[j])==1)
	                                    {
	                                       summation+=0.5+0.5*scoreTemp;
	                                    }
	                                    
	                                      
	                                    
	                                 }
	                                 //System.out.print("the last score : ");
	                                 //System.out.println((100*summation/Math.max(origin.length,template.length)));
	                                 logger.info("" + 100*summation/Math.max(origin.length,template.length));
	                                 if(Ms<(100*summation/Math.max(origin.length,template.length)))
	                                {
	                                    return 1;
	                             
	                                }
	                                 
	                              }
	                              
	                           }
	                           
	                        }                                 
	                     }   
	                  }
	               }
	            }
	         }
	         return 0;
	      }
	      
	   public static int isLocationMatched(double ori_x, double ori_y, double temp_x, double temp_y)
	   {
	      double thres=1;
	      
	      if (((temp_x-thres)<=ori_x)&&((temp_x+thres)>=ori_x)&&((temp_y-thres)<=ori_y)&&((temp_y+thres)>=ori_y))
	      {   
	         return 1;         
	      }
	      
	      else
	      {      
	         return 0;
	      }
	   }
	   
	   
	   public static double distancebetween(double x1, double y1, double x2, double y2) 
	   {
	      
	      return Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
	         
	   }
	   
	   public static boolean distancecompare(double a1, double a2, double thres) 
	   {
	      
	      if (Math.abs(a1-a2)<thres)
	      {
	         return true;
	      }
	      else
	      {
	         return false;
	      }
	   }
	   public static double rad2deg(double rad)
	   {
	      return (180.00/Math.PI)*rad;   
	   }
	   
	   
	   public static double deg2rad(double deg)
	   {
	      return deg*Math.PI/180.0;
	   }

	   
	   public static double[] locationChange(double x, double y, double[] abcd)
	   {
	      
	      double[] aa= {0,0};
	      aa[0]=x*abcd[0]+y*abcd[1]+abcd[2];
	      aa[1]=y*abcd[0]+(-1)*x*abcd[1]+abcd[3];
	      
	      return aa;
	      
	   }
	   
	   
	    public static double[][] multiplicar(double[][] A, double[][] B) {

	        int aRows = A.length;
	        int aColumns = A[0].length;
	        int bRows = B.length;
	        int bColumns = B[0].length;

	        if (aColumns != bRows) 
	        {
	            throw new IllegalArgumentException("A:Rows: " + aColumns + " did not match B:Columns " + bRows + ".");
	        }
	        

	        double[][] C = new double[aRows][bColumns];

	        for (int i = 0; i < aRows; i++) 
	        {
	            for (int j = 0; j < bColumns; j++) 
	            {
	                for (int k = 0; k < aColumns; k++) 
	                {
	                    C[i][j] += A[i][k] * B[k][j];
	                }
	            }
	        }

	        return C;
	    }
	    
	    
	   public static double[] locationChangeparam(double xx1, double yy1, double xx2, double yy2, double x1, double y1, double x2, double y2)
	   {
	      double[][] mat1 = {{xx1,yy1,1.0,0.0},{yy1,-xx1,0.0,1.0},{xx2,yy2,1.0,0.0},{yy2,-xx2,0.0,1.0}};
	      double[][] mat2 = {{x1},{y1},{x2},{y2}};
	      double[][] inv_mat1 = invert(mat1);
	      double[][] abcd = multiplicar(inv_mat1,mat2);
	      double[] abcd1={0,0,0,0};
	      
	      for (int i=0; i<4;i++)
	      {
	         abcd1[i]=abcd[i][0];   
	      }
	      
	      return abcd1;
	      
	   }
	          
	       
	   public static double[][] invert(double a[][]) 
	    {
	        int n = a.length;
	        double x[][] = new double[n][n];
	        double b[][] = new double[n][n];
	        int index[] = new int[n];
	        
	        for (int i=0; i<n; ++i)
	        {
	            b[i][i] = 1;
	        }
	        
	        gaussian(a, index);
	 
	        for (int i=0; i<n-1; ++i)
	        {
	           for (int j=i+1; j<n; ++j)
	           {
	                for (int k=0; k<n; ++k)
	                {
	                    b[index[j]][k]
	                           -= a[index[j]][i]*b[index[i]][k];
	                }
	           }
	        }
	        
	        for (int i=0; i<n; ++i) 
	        {
	            x[n-1][i] = b[index[n-1]][i]/a[index[n-1]][n-1];

	            for (int j=n-2; j>=0; --j) 
	            {
	                x[j][i] = b[index[j]][i];

	                for (int k=j+1; k<n; ++k) 
	                {
	                    x[j][i] -= a[index[j]][k]*x[k][i];
	                }                
	                x[j][i] /= a[index[j]][j];
	            }
	        }
	        return x;
	    }
	 
	 
	    public static void gaussian(double a[][], int index[]) 
	    {
	        int n = index.length;
	        double c[] = new double[n];
	 
	        for (int i=0; i<n; ++i) 
	            index[i] = i;
	 
	        for (int i=0; i<n; ++i) 
	        {
	            double c1 = 0;

	            for (int j=0; j<n; ++j) 
	            {
	                double c0 = Math.abs(a[i][j]);
	                if (c0 > c1) c1 = c0;
	            }
	            c[i] = c1;
	        }
	 
	        int k = 0;
	        for (int j=0; j<n-1; ++j) 
	        {
	            double pi1 = 0;
	            for (int i=j; i<n; ++i) 
	            {
	                double pi0 = Math.abs(a[index[i]][j]);
	                pi0 /= c[index[i]];
	                if (pi0 > pi1) 
	                {
	                    pi1 = pi0;
	                    k = i;
	                }
	            }
	 
	            int itmp = index[j];
	            index[j] = index[k];
	            index[k] = itmp;
	            for (int i=j+1; i<n; ++i)    
	            {
	                double pj = a[index[i]][j]/a[index[j]][j];
	 
	                a[index[i]][j] = pj;
	 
	                for (int l=j+1; l<n; ++l)
	                    a[index[i]][l] -= pj*a[index[j]][l];
	            }
	        }
	    }
	   
	}

	class FgFeature
	{
	   double distance;
	   double angle;
	   FgFeature(double distance, double angle)
	   {
	      this.distance=distance;
	      this.angle=angle;
	      
	   }
	}


	class Nearest2feature
	{
	   int x;
	   int y;
	   
	   int neighborhood1_x;
	   int neighborhood1_y;
	   int neighborhood2_x;
	   int neighborhood2_y;
	   
	   double distance_1;
	   double distance_2;
	   
	   double angle_me_1;
	   double angle_me_2;
	   
	   int type;
	   int type1;
	   int type2;

	   public Nearest2feature(int x, int y,int neighborhood1_x,int neighborhood1_y, int neighborhood2_x, int neighborhood2_y, double distance_1, double distance_2,double angle,double angle1,double angle2,int type, int type1, int type2 )
	   {
	      this.x=x;
	      this.y=y;
	      
	      this.neighborhood1_x=neighborhood1_x;
	      this.neighborhood1_y=neighborhood1_y;
	      this.neighborhood2_x=neighborhood2_x;
	      this.neighborhood2_y=neighborhood2_y;
	      
	      this.distance_1=distance_1;
	      this.distance_2=distance_2;
	      
	      this.angle_me_1=Math.abs(angle-angle1);
	      this.angle_me_2=Math.abs(angle-angle2);
	      
	      this.type=type;
	      this.type1=type1;
	      this.type2=type2;
	      
	      
	   }

   
}
