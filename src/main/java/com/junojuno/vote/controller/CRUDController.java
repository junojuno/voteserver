package com.junojuno.vote.controller;

import java.util.List;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.junojuno.vote.domain.JsonResponse;

public abstract class CRUDController<T, K, R, S> {
	S service;
	
	@ResponseBody
	public abstract R add(@ModelAttribute T entity);
	
	@ResponseBody
	public abstract R update(@ModelAttribute T entity);
	
	@ResponseBody
	public abstract R delete(@PathVariable K idx);
	
	@ResponseBody
	public abstract R view(@PathVariable K idx);  
	
	@ResponseBody
	public abstract R list();  
	
}
