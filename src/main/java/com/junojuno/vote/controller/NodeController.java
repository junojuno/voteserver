package com.junojuno.vote.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.junojuno.vote.domain.Hash;
import com.junojuno.vote.domain.JsonResponse;
import com.junojuno.vote.domain.Node;
import com.junojuno.vote.etc.Parser;
import com.junojuno.vote.service.HashService;
import com.junojuno.vote.service.NodeService;

@RequestMapping(value = "/node")
@Controller
public class NodeController {

	private static final Logger logger = LoggerFactory.getLogger(NodeController.class);
	
	@Resource(name = "nodeServiceImpl")
	NodeService nodeService;
	
	@Resource(name = "hashServiceImpl")
	HashService hashService;
	
	/**
	 * NODE
	 * @param node
	 * @return
	 */
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public JsonResponse add (Node node, HttpServletRequest request) {
		logger.info(request.getRemoteAddr() +" ?? " + node.getTail_hash());
		node.setAddress(request.getRemoteAddr());
		
		switch (nodeService.add(node)) {
			case 1:
				return new JsonResponse(node, true, "ok");
			default:
				return JsonResponse.errorResponse();
		}
	}
	
//	@ResponseBody
//	@RequestMapping(method = RequestMethod.PUT)
//	public JsonResponse update(@RequestBody String body) {
//		logger.info(body.toString() +" UPDATE ");
//		Map<String, String> params= Parser.requestBodyStringToMap(body);
//		com.junojuno.vote.etc.Logger.mapLog(logger, params);
//		
//		Node node = new Node(params.get("address"), params.get("tail_hash"));
//		
//		switch (nodeService.update(node)) {
//			case 1:
//				Hash latestHash = hashService.latest();
//				
//				HashMap<String, Boolean> map = new HashMap<String, Boolean>();
//				
//				if (latestHash.getHash().equals(node.getTail_hash())) {
//					map.put("is_latest", true);
//				} else {
//					map.put("is_latest", false);
//				}
//				
//				return new JsonResponse(map, true, "ok");
//			default:
//				return JsonResponse.errorResponse();
//		}
//	}
	
	@ResponseBody
	@RequestMapping(value = "/exist",method = RequestMethod.GET)
	public JsonResponse checkExistNode(Node node) {
		int cntNodes = nodeService.checkExistNode(node);
		
		HashMap<String, Boolean> map = new HashMap<String, Boolean>();
		map.put("is_exist_node", cntNodes == 1);
		
		return new JsonResponse(map, true, "ok");
	}
	
}
