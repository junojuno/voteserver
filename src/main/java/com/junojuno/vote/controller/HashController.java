package com.junojuno.vote.controller;

import java.util.HashMap;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.junojuno.vote.dao.ProbDao;
import com.junojuno.vote.dao.VoterDao;
import com.junojuno.vote.domain.Hash;
import com.junojuno.vote.domain.JsonResponse;
import com.junojuno.vote.service.HashService;
import com.junojuno.vote.service.HashServiceImpl;
import com.junojuno.vote.service.VoterService;

@RequestMapping("/hashs")
@Controller
public class HashController {

	private static final Logger logger = LoggerFactory.getLogger(HashController.class);
	

	@Resource(name = "hashServiceImpl")
	HashService hashService;

	@Resource(name = "voterServiceImpl")
	VoterService voterService;
	
	@Resource(name = "probDaoImpl")
	ProbDao probDao;
	
	@ResponseBody
	@RequestMapping(value ="/judge/res", method = RequestMethod.POST)
	public JsonResponse judgeRes(int prob, int isRight) {
		
		logger.info("judge Res / "  + isRight + " / prob");
		probDao.update(prob, isRight == 1);
		
		if (probDao.chkProbCanMakeBlock(prob) > 0) {
			return new JsonResponse(hashService.broadcastMakeBlock(prob), true, "ok");
		} else {
			return JsonResponse.errorResponse();
		}
	}
	
	
	@ResponseBody
	@RequestMapping(value ="/judge", method = RequestMethod.POST)
	public JsonResponse judge(Hash hash, int prob) {
		
		logger.info("judge "  + hash.getHash() + " / " + hash.getNonce() + " / " +hash.getPrev_hash() + " / " + prob);
		
		if (probDao.chkProbExist(prob) > 0) {
			return JsonResponse.errorResponse();
		}
		
		return new JsonResponse(hashService.judge(hash, prob), true, "ok");
		
	}
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public JsonResponse add(Hash hash) {
		
		// TODO : BROADCAST 
		hashService.notifyHashUpdated(hash);
		
		switch (hashService.add(hash)) {
			case 1:
				return new JsonResponse(hash, true, "ok");
			default:
				return JsonResponse.errorResponse();
		}
	}
	
	/**
	 * 최신 해시를 가져오면서, 해당 해싱
	 * @param hash
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/latest", method = RequestMethod.GET)
	public JsonResponse latest() {
		
		Hash result = hashService.latest();
		int voted = voterService.cntVoted();
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("hash", result == null ? "" : result.getHash());
		map.put("cnt_txes", voted);
		
		return new JsonResponse(map, true, "ok");
		
	}
}
