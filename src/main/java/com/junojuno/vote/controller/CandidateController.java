package com.junojuno.vote.controller;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.junojuno.vote.domain.Candidate;
import com.junojuno.vote.domain.Hash;
import com.junojuno.vote.domain.JsonResponse;
import com.junojuno.vote.domain.Voter;
import com.junojuno.vote.service.CandidateService;
import com.junojuno.vote.service.HashService;

@RequestMapping("/candidates")
@Controller
public class CandidateController {

	private static final Logger logger = LoggerFactory.getLogger(CandidateController.class);

	@Resource(name = "candidateServiceImpl")
	CandidateService candidateService;
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public JsonResponse add(@RequestParam("img") MultipartFile img, @RequestParam("name") String name, @RequestParam("promise") 
	String promise, @RequestParam("info") String info, @RequestParam("public_key") String public_key, @RequestParam("idx_elections") int idx_elections, HttpServletRequest request) {
		logger.info(img.hashCode() +" / " + name + "/" + request.getSession().getServletContext().getRealPath("/resources"));

		if (img != null) {
        	try {
                String fileName = img.getOriginalFilename();
            	File file = new File(request.getSession().getServletContext().getRealPath("/resources") + "/" + fileName);
				img.transferTo(file);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
		
		Candidate candidate = new Candidate();//(idx, name, promise, info, img, number, idx_elections)
		candidate.setName(name);
		candidate.setPromise(promise);
		candidate.setInfo(info);
		candidate.setImg(img.getOriginalFilename());
		candidate.setNumber(candidateService.lastNumber(idx_elections) + 1);
		candidate.setIdx_elections(idx_elections);
		candidate.setPublic_key(public_key);
		
		
		return new JsonResponse(candidateService.add(candidate), true, "ok");
		
	}
	
	@ResponseBody
	@RequestMapping(value = "/all",method = RequestMethod.GET)
	public JsonResponse all() {
		return new JsonResponse(candidateService.all(), true, "ok");
	}
	
	
}
