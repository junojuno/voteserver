package com.junojuno.vote.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.junojuno.vote.domain.JsonResponse;

@RequestMapping(value = "/info")
@Controller
public class InfoController {
	
	@ResponseBody
	@RequestMapping(value = "/alive",method = RequestMethod.GET)
	public JsonResponse vote() {
		return new JsonResponse(null, true, "ok");
	}
	
}