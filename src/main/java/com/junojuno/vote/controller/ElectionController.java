package com.junojuno.vote.controller;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.junojuno.vote.domain.Candidate;
import com.junojuno.vote.domain.Election;
import com.junojuno.vote.domain.JsonResponse;
import com.junojuno.vote.service.ElectionService;

@RequestMapping(value = "/elections")
@Controller
public class ElectionController {
	
	private static final Logger logger = LoggerFactory.getLogger(ElectionController.class);
	
	@Resource(name="electionServiceImpl")
	ElectionService electionService;

	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public JsonResponse add(Election election) {
		electionService.add(election);
		return new JsonResponse(electionService.latest(), true, "ok");
	}
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.GET)
	public JsonResponse list() {
		List<Election> result = electionService.selectElectionList();
		return new JsonResponse(result, true, "ok");
	}

	@ResponseBody
	@RequestMapping(value = "/{idx}/candidates", method = RequestMethod.GET)
	public JsonResponse candidates(@PathVariable Integer idx) {
		List<Candidate> result = electionService.selectElectionCandidateList(idx);
		return new JsonResponse(result, true, "ok");
	}
	
}