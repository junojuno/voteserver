package com.junojuno.vote.controller;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.junojuno.vote.domain.JsonResponse;
import com.junojuno.vote.domain.Voter;
import com.junojuno.vote.service.VoterService;

@RequestMapping(value = "/voter")
@Controller
public class VoterController {

	private static final Logger logger = LoggerFactory.getLogger(VoterController.class);
	
	@Resource(name = "voterServiceImpl")
	VoterService voterService;
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public JsonResponse add(Voter voter) {
		logger.info(voter.getFingerprint() + " / " + voter.getName());
		return new JsonResponse(voterService.add(voter), true, "ok");
	}

	@ResponseBody
	@RequestMapping(value = "/vote",method = RequestMethod.POST)
	public JsonResponse vote(String name, int candidate_idx, int election_idx,Voter voter, String sign) {
		logger.info("VOTED " + candidate_idx + " / " + election_idx + " / " +name);
		int resVote = voterService.vote(name, sign, election_idx, candidate_idx, voter.getFingerprint(), voter.getElections_idx(),  "TEST");
		
		if (resVote == -1) {
			return JsonResponse.errorResponse();
		}
		
		return new JsonResponse(resVote, true, "ok");
	}	
	
	@ResponseBody
	@RequestMapping(value = "/judge", method = RequestMethod.POST)
	public JsonResponse judge() {
		return JsonResponse.errorResponse();
	}
}
