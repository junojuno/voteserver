package com.junojuno.vote.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.junojuno.vote.domain.JsonResponse;
import com.junojuno.vote.service.NodeService;

@RequestMapping(value = "/restores")
@Controller
public class RestoreController {
	
	@Resource(name = "nodeServiceImpl")
	NodeService nodeService;
	
	@ResponseBody
	@RequestMapping(value = "/nodes", method = RequestMethod.GET)
	public JsonResponse getRestoreNodeAddress(HttpServletRequest request) {
		
		return new JsonResponse(nodeService.latestNodes(request.getRemoteAddr()), true, "ok");
	}
	
}