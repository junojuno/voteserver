package com.junojuno.vote.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.codehaus.jackson.map.util.JSONPObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.junojuno.vote.domain.JsonResponse;
import com.junojuno.vote.domain.Keypair;
import com.junojuno.vote.service.KeypairService;
import com.junojuno.vote.service.NodeService;

@RequestMapping(value = "/keypair")
@Controller
public class KeypairController {
	

	@Resource(name = "keypairServiceImpl")
	KeypairService keypairService;
	
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public JsonResponse addKeyPairList(int election_id, String keypair) {
		
		String[] keypairs = keypair.split(",");
		
		List<Keypair> keypairList = new ArrayList<Keypair>();
		
		for (int i = 0 ; i < keypairs.length ; i++) {
			keypairList.add(new Keypair(-1, keypairs[i], election_id));
		}
		
		return new JsonResponse(keypairService.addKeyPairList(keypairList), true, "ok");
	}
	
}