package com.junojuno.vote.service;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.junojuno.vote.dao.CandidateDao;
import com.junojuno.vote.dao.ElectionDao;
import com.junojuno.vote.domain.Candidate;
import com.junojuno.vote.domain.Election;
import com.junojuno.vote.domain.Node;

@Service("candidateServiceImpl")
public class CandidateServiceImpl implements CandidateService{

	@Resource(name="candidateDaoImpl")
	CandidateDao candidateDao;

	@Override
	public int add(Candidate candidate) {
		return candidateDao.add(candidate);
	}

	@Override
	public int lastNumber(int idx_elections) {
		return candidateDao.lastNumber(idx_elections);
	}

	@Override
	public List<Candidate> all() {
		return candidateDao.all();
	}

	@Override
	public Candidate who(int idx) {
		return candidateDao.who(idx);
	}

	
}
