package com.junojuno.vote.service;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.List;

import javax.annotation.Resource;

import org.codehaus.jackson.map.util.JSONPObject;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.junojuno.vote.controller.CandidateController;
import com.junojuno.vote.dao.NodeDao;
import com.junojuno.vote.dao.ProbDao;
import com.junojuno.vote.dao.VoterDao;
import com.junojuno.vote.domain.Keypair;
import com.junojuno.vote.domain.Node;
import com.junojuno.vote.domain.Prob;
import com.junojuno.vote.domain.Voter;
import com.junojuno.vote.etc.FingerMatcher;
import com.junojuno.vote.etc.Minutia;

@Service("voterServiceImpl")
public class VoterServiceImpl implements VoterService{

	private static final Logger logger = LoggerFactory.getLogger(VoterServiceImpl.class);
	
	private static final int PORT = 44444;


	private static final String COMMAND_NEW_PROB = "COMMAND_NEW_PROB";
	private static final String COMMAND_NEW_TRANSACTION = "COMMAND_NEW_TRANSACTION";

	private static final int PROB_GAP = 10;
	
	@Resource(name="candidateServiceImpl")
	CandidateService candidateService;

	@Resource(name="keypairServiceImpl")
	KeypairService keypairService;
		
	@Resource(name="nodeServiceImpl")
	NodeService nodeService;
	
	@Resource(name = "voterServiceImpl")
	VoterService voterService;

	@Resource(name = "voterDaoImpl")
	VoterDao voterDao;

	@Resource(name = "probDaoImpl")
	ProbDao probDao;
	
	@Override
	public int add(Voter voter) {
		return voterDao.add(voter);
	}
	
	@Override
	public int vote(String name, String sign, int election_idx, int candidate_idx, String fingerprint, int prev_id, String reg_date) {
		
		Voter voter = who(name);
		
		if (voter == null || voter.getFingerprint() == null) {
			return -1;
		}
		
		boolean isRightFinger = true;//isValidFinger(voter.getFingerprint(), fingerprint);
		
		logger.info("sign is " + sign );
		logger.info("Right Finger?" + isRightFinger);
		
		if (isValidVoter(voter) && isRightFinger) {
			List<Node> nodes = nodeService.list();
			
			int latestProb = probDao.getLatestProb();
						
			//int cntVoted = voterDao.cntVoted();

			JSONObject job = new JSONObject();
			//Keypair keypair = getUnUsedKeyPair(election_idx);
			
			//logger.info(keypair.getPrivate_key());
			//if (keypair == null || keypair.getPrivate_key() == null) {
			//	return -1;
			//}
			
			//keypairService.useKeypair(keypair.getId());
			voterService.updateVoteStatus(voter.getIdx());

			job.put("sign", sign);
			//job.put("private_key", keypair.getPrivate_key());
			job.put("prev_id", prev_id);
			job.put("reg_date", reg_date);
			String candKey = candidateService.who(candidate_idx).getPublic_key();
			logger.info("CANDKEY =" + candKey);
			job.put("candidate", candKey);
			
			
			for (Node node : nodes) {
				sendNotifyMsg(node.getAddress(), COMMAND_NEW_TRANSACTION);
				sendNotifyMsg(node.getAddress(), job.toString());
			}
			
			
		
			for (Node node : nodes) {
				sendNotifyMsg(node.getAddress(), COMMAND_NEW_PROB);
				sendNotifyMsg(node.getAddress(), latestProb + 1 + ""); // start
				sendNotifyMsg(node.getAddress(), 1 + ""); // tick
			}
			
			
			return nodes.size();
		}
		
		return -1;
	}
	

	private boolean isValidVoter(Voter voter) {
		logger.info(voter.getName());
		return voter != null && !voter.getName().isEmpty();
	}

	private Keypair getUnUsedKeyPair(int election_idx) {
		return keypairService.getUnUsedKeyPair(election_idx);
	}

	private boolean isValidFinger(String voterFingerprint, String srcFingerprint) {

		Minutia[] voters = getMinuArray(new JSONArray(voterFingerprint));
		Minutia[] srcs = getMinuArray(new JSONArray(srcFingerprint));		
		
		return FingerMatcher.minutiaeMatching(voters, srcs, 6000) == 1 ? true : false;
	}
	
	private Minutia[] getMinuArray(JSONArray jar) {
		
		Minutia[] voter = new Minutia[jar.length()];
		
		for (int i = 0 ; i < jar.length() ; i++) {
			JSONObject job = jar.getJSONObject(i);
			
			voter[i] = new Minutia((int)(job.getDouble("x")), (int)(job.getDouble("y")), job.getString("MinutiaType").equals("end") ? 0 : 1,job.getDouble("angle"));
		}
		
		return voter;
	}

	public void sendNotifyMsg(String address, String msg) {
		try {
			
			DatagramSocket socket = new DatagramSocket();
			byte buffer[] = msg.getBytes();
			
			InetAddress inetAddress = InetAddress.getByName(address);
			
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length, inetAddress, PORT);
			socket.send(packet);
			
			socket.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int updateVoteStatus(int idx) {
		return voterDao.updateVoteStatus(idx);
	}

	@Override
	public Voter who(String name) {
		return voterDao.who(name);
	}

	@Override
	public int cntVoted() {
		return voterDao.cntVoted();
	}
}
