package com.junojuno.vote.service;

import java.util.List;

import com.junojuno.vote.domain.Candidate;
import com.junojuno.vote.domain.Election;
import com.junojuno.vote.domain.Node;

public interface NodeService {
	public int add(Node node);
	public int update(Node node);
	public List<Node> latestNodes(String addr);
	public int checkExistNode(Node node);
	public List<Node> list();
}
