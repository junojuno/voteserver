package com.junojuno.vote.service;

import com.junojuno.vote.domain.Hash;

public interface HashService {
	public int add(Hash hash);
	public Hash latest();
	public int judge(Hash hash, int prob);
	public void notifyHashUpdated(Hash hash);
	public int broadcastMakeBlock(int prob);
}
