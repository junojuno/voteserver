package com.junojuno.vote.service;

import java.security.KeyPair;
import java.util.List;

import com.junojuno.vote.domain.Candidate;
import com.junojuno.vote.domain.Election;
import com.junojuno.vote.domain.Keypair;
import com.junojuno.vote.domain.Node;

public interface KeypairService {
	public int addKeyPairList(List<Keypair> keypairDist);
	public Keypair getUnUsedKeyPair(int election_id);
	public int useKeypair(int id);
}
