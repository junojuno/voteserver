	package com.junojuno.vote.service;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.junojuno.vote.controller.NodeController;
import com.junojuno.vote.dao.HashDao;
import com.junojuno.vote.dao.ProbDao;
import com.junojuno.vote.domain.Hash;
import com.junojuno.vote.domain.Node;
import com.junojuno.vote.domain.Prob;

@Service("hashServiceImpl")
public class HashServiceImpl implements HashService {
	
	private static final String COMMAND_JUDGE = "COMMAND_JUDGE";
	private static final String COMMAND_NEW_BLOCK = "COMMAND_NEW_BLOCK";
	//private static final String COMMAND_NEW_TRANSACTION = "COMMAND_NEW_TRANSACTION";
	
	private static final Logger logger = LoggerFactory.getLogger(HashServiceImpl.class);

	@Resource(name = "hashDaoImpl")
	HashDao hashDao;
	
	@Resource(name = "nodeServiceImpl")
	NodeService nodeService;
	

	@Resource(name = "probDaoImpl")
	ProbDao probDao;
	
	@Override
	public int add(Hash hash) {
		return hashDao.add(hash);
	}

	@Override
	public Hash latest() {
		return hashDao.latest();
	}

	@Override
	public void notifyHashUpdated(Hash hash) {
		List<Node> nodeList = nodeService.list();
		JSONObject obj = transToJobject(hash);
				
		for (Node node : nodeList) {
			sendNotifyMsg(node.getAddress(), obj.toString());
		}
	}

	private JSONObject transToJobject(Hash hash) {
		JSONObject obj = new JSONObject();

		if (hash == null) {
			return obj;
		}
		
		if (hash.getHash() != null && !hash.getHash().isEmpty()) {
			obj.put("hash", hash.getHash());
		}
		
		if (hash.getNonce() != null && !hash.getNonce().isEmpty()) {
			obj.put("nonce", hash.getNonce());
		}
		
		if (hash.getPrev_hash() != null && !hash.getPrev_hash().isEmpty()) {
			obj.put("prev_hash", hash.getPrev_hash());
		}
		return obj;
	}
	
	public void sendNotifyMsg(String address, String msg) {
		try {
			DatagramSocket socket = new DatagramSocket();
			byte buffer[] = msg.getBytes();
			
			InetAddress inetAddress = InetAddress.getByName(address);
			
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length, inetAddress, 44444);
			socket.send(packet);
			
			socket.close();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public int judge(Hash hash, int prob) {
		
		List<Node> nodeList = nodeService.list();
		JSONObject obj = transToJobject(hash);
		obj.put("prob", prob);
				
		for (Node node : nodeList) {
			logger.info("COMMAND JUDEGE " + obj);
			
			sendNotifyMsg(node.getAddress(), COMMAND_JUDGE);
			sendNotifyMsg(node.getAddress(), obj.toString());
		}
		

		Prob probObj = new Prob();
		probObj.setProb(prob);
		probObj.setTotal(nodeList.size());
		probObj.setHash(hash.getHash());
		probDao.add(probObj);
		
		return nodeList.size();
	}


	@Override
	public int broadcastMakeBlock(int prob) {
		//String prevHash, String hash, 
		
		probDao.updateStatusCantMakeBlock(prob);
		String hash = probDao.prob(prob).getHash();
		String prevHash = hashDao.latest().getHash();

		Hash hashObj = new Hash();
		hashObj.setHash(hash);
		hashDao.add(hashObj);
		
		logger.info("BoradCast Hash / PrevHash " +hash + " / " + prevHash);
		
		List<Node> nodeList = nodeService.list();
		JSONObject job = new JSONObject();
		job.put("prev_hash", prevHash);
		job.put("hash", hash);
		job.put("prob", prob);
				
		for (Node node : nodeList) {
			sendNotifyMsg(node.getAddress(), COMMAND_NEW_BLOCK);
			sendNotifyMsg(node.getAddress(), job.toString());
		}
		
		return nodeList.size();
	}

}
