package com.junojuno.vote.service;

import java.util.List;

import com.junojuno.vote.domain.Candidate;
import com.junojuno.vote.domain.Election;

public interface CandidateService {
	public int add(Candidate candidate);
	public int lastNumber(int idx_elections);
	public List<Candidate> all();
	public Candidate who(int idx);
}
