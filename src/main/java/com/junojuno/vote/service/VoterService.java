package com.junojuno.vote.service;

import java.util.List;

import com.junojuno.vote.domain.Candidate;
import com.junojuno.vote.domain.Election;
import com.junojuno.vote.domain.Node;
import com.junojuno.vote.domain.Voter;

public interface VoterService {
	public int add(Voter voter);
	public int vote(String name, String sign, int election_idx, int candidate_idx, String fingerprint, int prev_id, String reg_date);
	public int updateVoteStatus(int idx);
	public Voter who(String name);
	public int cntVoted();
}
