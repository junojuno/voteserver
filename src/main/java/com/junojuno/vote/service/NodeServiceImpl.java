package com.junojuno.vote.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.junojuno.vote.dao.NodeDao;
import com.junojuno.vote.domain.Node;

@Service("nodeServiceImpl")
public class NodeServiceImpl implements NodeService{

	@Resource(name = "nodeDaoImpl")
	NodeDao nodeDao;
	
	@Override
	public int add(Node node) {
		return nodeDao.add(node);
	}

	@Override
	public int update(Node node) {
		return nodeDao.update(node);
	}

	@Override
	public List<Node> latestNodes(String addr) {
		return nodeDao.latestNodes(addr);
	}

	@Override
	public int checkExistNode(Node node) {
		return nodeDao.checkExistNode(node);
	}

	@Override
	public List<Node> list() {
		return nodeDao.list();
	}

}
