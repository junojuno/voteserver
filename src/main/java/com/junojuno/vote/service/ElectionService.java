package com.junojuno.vote.service;

import java.util.List;

import com.junojuno.vote.domain.Candidate;
import com.junojuno.vote.domain.Election;

public interface ElectionService {
	public int add(Election election);
	public Election latest();
	public List<Election> selectElectionList();
	public List<Candidate> selectElectionCandidateList(int idx_elections);
}
