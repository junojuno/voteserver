package com.junojuno.vote.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.junojuno.vote.dao.ElectionDao;
import com.junojuno.vote.domain.Candidate;
import com.junojuno.vote.domain.Election;

@Service("electionServiceImpl")
public class ElectionServiceImpl implements ElectionService{
	
	@Resource(name="electionDaoImpl")
	ElectionDao electionDao;

	@Override
	public List<Election> selectElectionList() {
		return electionDao.selectElectionList();
	}

	@Override
	public List<Candidate> selectElectionCandidateList(int idx_elections) {
		return electionDao.selectElectionCandidateList(idx_elections);
	}

	@Override
	public int add(Election election) {
		return electionDao.add(election);
	}

	@Override
	public Election latest() {
		return electionDao.latest();
	}

}
