package com.junojuno.vote.service;

import java.security.KeyPair;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.junojuno.vote.dao.KeypairDao;
import com.junojuno.vote.dao.NodeDao;
import com.junojuno.vote.domain.Keypair;
import com.junojuno.vote.domain.Node;

@Service("keypairServiceImpl")
public class KeypairServiceImpl implements KeypairService{

	@Resource(name = "keypairDaoImpl")
	KeypairDao keypairDao;

	@Override
	public int addKeyPairList(List<Keypair> keypairList) {
		return keypairDao.addKeyPairList(keypairList);
	}

	@Override
	public Keypair getUnUsedKeyPair(int election_id) {
		return keypairDao.getUnUsedKeyPair(election_id);
	}

	@Override
	public int useKeypair(int id) {
		return keypairDao.useKeypair(id);
	}

}
