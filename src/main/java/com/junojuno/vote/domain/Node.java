package com.junojuno.vote.domain;

public class Node {
	private String address;
	private String tail_hash;

	public Node() {
		
	}

	public Node(String address, String tail_hash) {
		super();
		this.address = address;
		this.tail_hash = tail_hash;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}	
	
	public String getTail_hash() {
		return tail_hash;
	}

	public void setTail_hash(String tail_hash) {
		this.tail_hash = tail_hash;
	}
	
}
