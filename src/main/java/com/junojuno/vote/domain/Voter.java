package com.junojuno.vote.domain;

public class Voter {
	private int idx;
	private String name;
	private String fingerprint;
	//private FingerPrint fingerprint;
	private boolean isVote;
	private int power;
	private int elections_idx;

	public Voter() {
		
	}
	
	public Voter(int idx, String name, String fingerprint, boolean isVote, int power, int elections_idx) {
		super();
		this.idx = idx;
		this.name = name;
		this.fingerprint = fingerprint;
		this.isVote = isVote;
		this.power = power;
		this.elections_idx = elections_idx;
	}

	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFingerprint() {
		return fingerprint;
	}

	public void setFingerprint(String fingerprint) {
		this.fingerprint = fingerprint;
	}

	public boolean isVote() {
		return isVote;
	}

	public void setVote(boolean isVote) {
		this.isVote = isVote;
	}
	
	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public int getElections_idx() {
		return elections_idx;
	}

	public void setElections_idx(int elections_idx) {
		this.elections_idx = elections_idx;
	}
}
