package com.junojuno.vote.domain;

public class Hash {
	private int idx;
	private String hash;
	private String prev_hash;
	private String nonce;
	
	public Hash() {
	}
	
	public Hash(int idx, String hash, String prev_hash, String nonce) {
		this.idx = idx;
		this.hash = hash;
		this.prev_hash = prev_hash;
		this.nonce = nonce;
	}

	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getPrev_hash() {
		return prev_hash;
	}

	public void setPrev_hash(String prev_hash) {
		this.prev_hash = prev_hash;
	}

	public String getNonce() {
		return nonce;
	}

	public void setNonce(String nonce) {
		this.nonce = nonce;
	}
	
	
}
