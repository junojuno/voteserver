package com.junojuno.vote.domain;

public class Candidate {
	private int idx;
	private String name;
	private String promise;
	private String info;
	private String img;
	private int number;
	private int idx_elections;
	private String public_key;
	
	public Candidate() {
		
	}
	
	public Candidate(int idx, String name, String promise, String info, String img, int number, int idx_elections, String public_key) {
		super();
		this.idx = idx;
		this.name = name;
		this.promise = promise;
		this.info = info;
		this.img = img;
		this.number = number;
		this.idx_elections = idx_elections;
		this.public_key = public_key;
	}

	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPromise() {
		return promise;
	}

	public void setPromise(String promise) {
		this.promise = promise;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}
	
	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getPublic_key() {
		return public_key;
	}

	public void setPublic_key(String public_key) {
		this.public_key = public_key;
	}

	public int getIdx_elections() {
		return idx_elections;
	}

	public void setIdx_elections(int idx_elections) {
		this.idx_elections = idx_elections;
	}


}
