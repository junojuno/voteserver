package com.junojuno.vote.domain;

public class Keypair {
	private int id;
	private String private_key;
	private int election_id;

	public Keypair() {
		
	}
	
	public Keypair(int id, String private_key, int election_id) {
		this.id = id;
		this.private_key = private_key;
		this.election_id = election_id;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPrivate_key() {
		return private_key;
	}

	public void setPrivate_key(String private_key) {
		this.private_key = private_key;
	}

	public int getElection_id() {
		return election_id;
	}

	public void setElection_id(int election_id) {
		this.election_id = election_id;
	}

}
