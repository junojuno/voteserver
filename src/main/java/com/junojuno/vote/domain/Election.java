package com.junojuno.vote.domain;

public class Election {
	private int idx;
	private ElectionType type;
	private String title;
	private String state;
	
	public Election() {
		
	}
	
	public Election(int idx, String title, ElectionType type, String state) {
		super();
		this.idx = idx;
		this.title = title;
		this.type = type;
		this.state = state;
	}

	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public ElectionType getType() {
		return type;
	}

	public void setType(ElectionType type) {
		this.type = type;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	
}

