package com.junojuno.vote.domain;

public class Prob {
	private int idx;
	private int prob;
	private int total;
	private int ok;
	private int no;
	private int maked;
	private String hash;
	
	public Prob() {
		
	}
	
	public Prob(int idx, int prob, int total, int ok, int no, int maked, String hash) {
		this.idx = idx;
		this.prob = prob;
		this.total = total;
		this.ok = ok;
		this.no = no;
		this.maked = maked;
		this.hash = hash;
	}
	
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public int getProb() {
		return prob;
	}
	public void setProb(int prob) {
		this.prob = prob;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getOk() {
		return ok;
	}

	public void setOk(int ok) {
		this.ok = ok;
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public int getMaked() {
		return maked;
	}

	public void setMaked(int maked) {
		this.maked = maked;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

}
