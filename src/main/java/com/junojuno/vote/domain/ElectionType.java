package com.junojuno.vote.domain;

public enum ElectionType {
	GENERAL(1), PRESIDENTIAL(2);
	
	private int value;
	
	private ElectionType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	public static ElectionType valueOf(int value) {
		switch(value) {
		case 1: return GENERAL;
		case 2: return PRESIDENTIAL;
		}

		throw new IllegalArgumentException("잘못된 type value입니다");
	}
}
