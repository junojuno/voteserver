package com.junojuno.vote.domain;

public class JsonResponse {
	private Object data;
	private boolean success;
	private String msg;
	
	private static JsonResponse errorResponse;
	
	public static JsonResponse errorResponse() {
		if (errorResponse == null) {
			errorResponse = new JsonResponse();
		}
		
		return errorResponse;
	}
	
	public JsonResponse() {
		this.data = null;
		this.success = false;
		this.msg = "error";
	}
	
	public JsonResponse(Object data, boolean success, String msg) {
		super();
		this.data = data;
		this.success = success;
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
}
