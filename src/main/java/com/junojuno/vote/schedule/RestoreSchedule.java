package com.junojuno.vote.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class RestoreSchedule {
	
	private static final Logger logger = LoggerFactory.getLogger(RestoreSchedule.class);
	
	@Scheduled(fixedDelay = 5000)
	public void updateRestoreNode() {
		logger.info("sch");
	}
	
}
