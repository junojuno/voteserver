package com.junojuno.vote.dao;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.junojuno.vote.controller.ElectionController;
import com.junojuno.vote.domain.Candidate;
import com.junojuno.vote.domain.Election;

@Repository("candidateDaoImpl")
public class CandidateDaoImpl implements CandidateDao {

	private static final Logger logger = LoggerFactory.getLogger(CandidateDaoImpl.class);
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Override
	public int add(Candidate candidate) {
		return sqlSession.insert("candidate.add", candidate);
	}

	@Override
	public int lastNumber(int idx_elections) {
		return sqlSession.selectOne("candidate.last_number", idx_elections);
	}

	@Override
	public List<Candidate> all() {
		return sqlSession.selectList("candidate.all");
	}

	@Override
	public Candidate who(int idx) {
		return sqlSession.selectOne("candidate.who", idx);
	}

}
