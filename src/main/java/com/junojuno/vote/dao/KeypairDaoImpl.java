package com.junojuno.vote.dao;

import java.security.KeyPair;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.junojuno.vote.domain.Candidate;
import com.junojuno.vote.domain.Election;
import com.junojuno.vote.domain.Keypair;

@Repository("keypairDaoImpl")
public class KeypairDaoImpl implements KeypairDao {

	@Autowired
	SqlSessionTemplate sqlSessionTemplate;

	@Override
	public int addKeyPairList(List<Keypair> keypairList) {
		
		int res = 0;
		
		for(Keypair keypair : keypairList) {
			res += sqlSessionTemplate.insert("keypair.add_key_pair", keypair);
		}
		
		return res;
	}

	@Override
	public Keypair getUnUsedKeyPair(int election_id) {
		return sqlSessionTemplate.selectOne("keypair.get_unused_key_pair", election_id);
	}

	@Override
	public int useKeypair(int id) {
		return sqlSessionTemplate.update("keypair.use_keypair", id);
	}
	
}
