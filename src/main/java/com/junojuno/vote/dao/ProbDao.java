package com.junojuno.vote.dao;

import com.junojuno.vote.domain.Prob;

public interface ProbDao {
	public int getLatestProb();
	public int add(Prob prob);
	public int update(int prob, boolean isOk);
	public int chkProbExist(int prob);
	public int chkProbCanMakeBlock(int prob);
	public Prob prob(int prob);
	public int updateStatusCantMakeBlock(int prob);
}
