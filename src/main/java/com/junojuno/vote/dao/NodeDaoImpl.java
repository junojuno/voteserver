package com.junojuno.vote.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.junojuno.vote.domain.Node;

@Repository("nodeDaoImpl")
public class NodeDaoImpl implements NodeDao{
	
	@Autowired
	SqlSessionTemplate sqlSessionTemplate;
	
	@Override
	public int add (Node node) {
		return sqlSessionTemplate.insert("node.add", node);
	}

	@Override
	public int update(Node node) {
		return sqlSessionTemplate.update("node.update", node);
	}

	@Override
	public List<Node> latestNodes(String addr) {
		return sqlSessionTemplate.selectList("node.latest_nodes", addr);
	}

	@Override
	public int checkExistNode(Node node) {
		return sqlSessionTemplate.selectOne("node.check_exist_node", node);
	}

	@Override
	public List<Node> list() {
		return sqlSessionTemplate.selectList("node.list");
	}
}
