package com.junojuno.vote.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.junojuno.vote.domain.Node;
import com.junojuno.vote.domain.Voter;

@Repository("voterDaoImpl")
public class VoterDaoImpl implements VoterDao {

	@Autowired
	SqlSessionTemplate sqlSessionTemplate;
	
	@Override
	public int add(Voter voter) {
		return sqlSessionTemplate.insert("voter.add", voter);
	}

	@Override
	public int updateVoteStatus(int idx) {
		return sqlSessionTemplate.update("voter.updae_vote_status", idx);
	}

	@Override
	public Voter who(String name) {
		return sqlSessionTemplate.selectOne("voter.who", name);
	}

	@Override
	public int cntVoted() {
		return sqlSessionTemplate.selectOne("voter.cnt_voted");
	}
}
