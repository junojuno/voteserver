package com.junojuno.vote.dao;

import java.util.List;

import com.junojuno.vote.domain.Node;

public interface NodeDao {
	public int add (Node node);
	public int update(Node node);
	public List<Node> latestNodes(String addr);
	public int checkExistNode(Node node);
	public List<Node> list();
}
