package com.junojuno.vote.dao;

import java.util.List;

import com.junojuno.vote.domain.Node;
import com.junojuno.vote.domain.Voter;

public interface VoterDao {
	public int add (Voter voter);
	public int updateVoteStatus(int idx);
	public Voter who(String name);
	public int cntVoted();
}
