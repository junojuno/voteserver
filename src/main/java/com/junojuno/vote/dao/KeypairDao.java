package com.junojuno.vote.dao;

import java.util.List;
import java.util.Map;

import com.junojuno.vote.domain.Candidate;
import com.junojuno.vote.domain.Election;
import com.junojuno.vote.domain.Keypair;

public interface KeypairDao {
	public int addKeyPairList(List<Keypair> keypairDist);
	public Keypair getUnUsedKeyPair(int election_id);
	public int useKeypair(int id);
}
