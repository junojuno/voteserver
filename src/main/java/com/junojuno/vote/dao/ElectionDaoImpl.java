package com.junojuno.vote.dao;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.junojuno.vote.controller.ElectionController;
import com.junojuno.vote.domain.Candidate;
import com.junojuno.vote.domain.Election;

@Repository("electionDaoImpl")
public class ElectionDaoImpl implements ElectionDao {

	private static final Logger logger = LoggerFactory.getLogger(ElectionDaoImpl.class);
	
	@Autowired
	private SqlSessionTemplate sqlSession;

	protected void printQueryId(String queryId) {
        if(logger.isDebugEnabled()){
        	logger.debug("\t QueryId  \t:  " + queryId);
        }
    }
	
	@Override
	public List<Election> selectElectionList() {
		return sqlSession.selectList("election.selectElectionList");
	}

	@Override
	public List<Candidate> selectElectionCandidateList(int idx_elections) {
		return sqlSession.selectList("election.selectElectionCandidateList", idx_elections);
	}

	@Override
	public int add(Election election) {
		return sqlSession.insert("election.add", election);
	}

	@Override
	public Election latest() {
		return sqlSession.selectOne("election.latest");
	}

}
