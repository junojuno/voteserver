package com.junojuno.vote.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.junojuno.vote.domain.Node;
import com.junojuno.vote.domain.Prob;
import com.junojuno.vote.domain.Voter;

@Repository("probDaoImpl")
public class ProbDaoImpl implements ProbDao {

	@Autowired
	SqlSessionTemplate sqlSessionTemplate;

	@Override
	public int getLatestProb() {
		return sqlSessionTemplate.selectOne("prob.latest");
	}

	@Override
	public int add(Prob prob) {
		return sqlSessionTemplate.insert("prob.add", prob);
	}

	@Override
	public int update(int prob, boolean isOk) {
		
		if (isOk) {
			return sqlSessionTemplate.update("prob.judge_res_ok", prob);
		} else {
			return sqlSessionTemplate.update("prob.judge_res_no", prob);
		}
		
	}

	@Override
	public int chkProbExist(int prob) {
		return sqlSessionTemplate.selectOne("prob.chk_prob_exist", prob);
	}

	@Override
	public int chkProbCanMakeBlock(int prob) {
		return sqlSessionTemplate.selectOne("prob.chk_prob_can_make_block", prob);
	}

	@Override
	public Prob prob(int prob) {
		return sqlSessionTemplate.selectOne("prob.prob", prob);
	}

	@Override
	public int updateStatusCantMakeBlock(int prob) {
		return sqlSessionTemplate.update("prob.update_prob_cant_make_block", prob);
	}
	
}
