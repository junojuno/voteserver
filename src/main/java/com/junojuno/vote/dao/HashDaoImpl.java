package com.junojuno.vote.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.junojuno.vote.domain.Hash;

@Repository("hashDaoImpl")
public class HashDaoImpl implements HashDao {

	@Autowired
	SqlSessionTemplate sqlSessionTemplate;
	
	@Override
	public int add(Hash hash) {
		return sqlSessionTemplate.insert("hash.add", hash);
	}

	@Override
	public Hash latest() {
		return sqlSessionTemplate.selectOne("hash.latest");
	}

}
