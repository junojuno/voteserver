package com.junojuno.vote.dao;

import com.junojuno.vote.domain.Hash;

public interface HashDao {
	public int add(Hash hash);
	public Hash latest(); 
}
