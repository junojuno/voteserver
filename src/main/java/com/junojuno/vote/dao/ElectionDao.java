package com.junojuno.vote.dao;

import java.util.List;
import java.util.Map;

import com.junojuno.vote.domain.Candidate;
import com.junojuno.vote.domain.Election;

public interface ElectionDao {
	public int add(Election election);
	public Election latest();
	public List<Election> selectElectionList();
	public List<Candidate> selectElectionCandidateList(int idx_elections);
}
